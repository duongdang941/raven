# Build Docker
## Cài đặt
Clone project to web folder, Source code trong thư mục /web:
```
git clone {git_url} web
```
Chạy command:

```shell
docker-compose build
docker-compose up -d
```

Default database access from host:
```
mysql -u root -p -P 6600 -h 127.0.0.1
Port: 6600
Username: root
Password: root
```

Tạo databases:
```shell
create database db_ravencrestbotanicals;
exit;
```

Sửa file host `/etc/hosts` nếu dùng virtual host `wgtRaven.local`


Cài php package bằng composer (chú ý ssh vào container trước) :
```shell
docker-compose exec -u www-data php bash
composer install
```

## Build Laravel (chú ý ssh vào container trước)
1) Tạo file .env
```shell
cp .env.exampe .env
```

2) Tạo key laravel 
```shell
php artisan key:generate
```

3) Migrating DB Schema
```shell
php artisan migrate --seed
```

## Thông tin đăng nhập
account/pass: admin@gmail.com/admin@123

## Hướng dẫn sử dụng

Chạy docker :

```shell
docker-compose up -d
```

Dừng docker :

```shell
docker-compose stop
```

## Reference shopify document:
1. https://help.shopify.com/en/manual/orders/notifications/webhooks
2. https://shopify.dev/docs/api/admin-rest/2023-04/resources/webhook#event-topics
3. https://hookdeck.com/cli