<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('/admin');
});

Route::group([
    'prefix' => config('backpack.base.route_prefix'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web')
    ),
], function () {
//    Route::get('login', 'Auth\LoginController@showLoginForm')->name('backpack.auth.login');
//    Route::post('login', 'Auth\LoginController@login');
//    Route::get('logout', 'Auth\LoginController@logout');

//    Route::get('dashboard', 'Admin\AdminController@dashboard')->name('backpack.dashboard');
//    Route::get('/', 'Admin\AdminController@redirect')->name('backpack');
});
