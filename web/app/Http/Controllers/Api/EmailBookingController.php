<?php

namespace App\Http\Controllers\Api;

use App\Interfaces\EmailBookingInterface;
use App\Mail\EmailBooking;
use App\Models\Bookmettings;
use App\Models\GeneralSettings;
use App\Services\EmailBookingService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailBookingController extends ApiController
{
    /**
     * @var EmailBookingService
     */
    protected $bookingService;

    /**
     * @var EmailBookingInterface
     */
    protected $bookingInterface;

    /**
     * @param EmailBookingInterface $bookingInterface
     * @param EmailBookingService $bookingService
     */
    public function __construct(
        EmailBookingInterface $bookingInterface ,
        EmailBookingService $bookingService
    ) {
        $this->bookingInterface = $bookingInterface;
        $this->bookingService   = $bookingService;
    }

    /**
     * Function handle send email.
     *
     * @param $email
     * @param $type
     * @param $data
     * @return JsonResponse
     */
    public function sendMail($email, $type, $data) {
        try {
            Mail::to($email)->send(new EmailBooking($type, $data));

            return $this->sendResponse($data, 'Email send Successfully!');
        } catch (\Exception $exception) {

            return $this->sendError('Something went wrong!' . $exception->getMessage());
        }
    }

    /**
     * Send email when user submit form schedule.
     *
     * @return JsonResponse
     */
    public function preSchedule(Request $request) {
        try {
            $email = $request->input('email');
            $data = [];

            if ($email) {
                $url = GeneralSettings::where('setting_key', 'schedule_product_url')->first();
                $data['url'] = $url['setting_value'];

                $this->sendMail($email, Bookmettings::STATUS_BEFORE_BOOKING, $data);

                return $this->sendResponse($url, 'Email send Successfully!');
            }
        } catch (\Exception $exception) {
            return $this->sendError('Something went wrong!' . $exception->getMessage());
        }
    }

    /**
     * Schedule a consultation
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function schedule(Request $request) {
        try {
            $data = [];
            $dataWebhook = $this->receiveWebhookOrderPaid($request);
            $daysForSchedule = GeneralSettings::where('setting_key', GeneralSettings::DAYS_AVAILABLE_SCHEDULE)->first();
            $intakeFormUrl = GeneralSettings::where('setting_key', GeneralSettings::INTAKE_FORM_URL)->first();
            $dateMeting = Carbon::now()->addDays($daysForSchedule->getAttribute('setting_value'));

            if (!empty($dataWebhook) && $dataWebhook['financial_status'] == "paid") {
                $data['email'] = $dataWebhook['email'] ?? '';
                $data['date_meting'] = $dateMeting ?? Carbon::now()->addDays(Bookmettings::DEFAULT_DAYS_SCHEDULE);

                $this->store($data);

                $data['linkReschedule'] = env('APP_URL') . '/schedule-form/?' . $data['email'] ;
                $data['googleFormUrl']  = $intakeFormUrl->getAttribute('setting_value');

                $this->sendMail($data['email'], Bookmettings::STATUS_SUBMITTED_BOOKING, $data);
            }

            return $this->sendResponse($request, 'Email send Successfully');
        } catch (\Exception $exception) {

            return $this->sendError('Something went wrong ' . $exception->getMessage());
        }
    }

    /**
     * Schedule a consultation
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function reschedule(Request $request) {
        $data = [];
        try {
            $daysForSchedule = GeneralSettings::where('setting_key', 'days_available_schedule')->first()
                                ?? Bookmettings::DEFAULT_DAYS_SCHEDULE;

            $requestData = $request->all();
            $data['email'] = $requestData['verifyEmail'] ?? '';
            $data['date_meting'] = $requestData['datetime']
                                        ? date('Y-m-d H:i:s', strtotime($requestData['datetime']))
                                        : Carbon::now()->addDays($daysForSchedule);

            if(!empty($data['email']) && $this->checkAvailableReschedule($data)) {
                $this->update($data);

                $this->sendMail($data['email'], Bookmettings::STATUS_RESUBMITTED_BOOKING, $data);
            }

            return $this->sendResponse($request, 'Email send Successfully');
        } catch (\Exception $exception) {

            return $this->sendError('Something went wrong ' . $exception->getMessage());
        }
    }

    /**
     * Check if schedule available for reschedule
     *
     * @param $data
     * @return bool|void
     */
    public function checkAvailableReschedule($data) {
        $scheduled = $this->getActiveScheduled($data);
        $now = Carbon::now();
        $settingDaysForSchedule = GeneralSettings::where('setting_key', 'days_available_schedule')
                                                ->where('status', Bookmettings::STATUS_NEW)
                                                ->first();
        $availableDaysForSchedule = !empty($settingDaysForSchedule)
                                    ? $settingDaysForSchedule->getAttribute('setting_value')
                                    : Bookmettings::DEFAULT_DAYS_SCHEDULE;
        $availableSettingTime = $now->diffInMinutes($now->copy()->addDays($availableDaysForSchedule));

        if($scheduled) {
            $now = Carbon::now();
            $timeBooking = Carbon::create($data['date_meting']);

            $availableTime = $now->diffInMinutes($timeBooking, false);

            if ($availableTime < 0 ) {
                $this->cancel($data);
            } else {
                return $availableTime >= $availableSettingTime;
            }
        } else {
            return false;
        }
    }

    /**
     * Create schedule
     *
     * @param $data
     * @return void
     */
    public function store($data) {
        Bookmettings::create([
            'email' => $data['email'],
            'date_meting' => $data['date_meting'],
            'status' => Bookmettings::STATUS_NEW,
        ]);
    }

    /**
     * Update data schedule
     *
     * @param $data
     * @return void
     */
    public function update($data) {
        $scheduled = $this->getActiveScheduled($data);

        if($scheduled) {
            $scheduled->update([
                'date_meting' => $data['date_meting'],
                'status' => Bookmettings::STATUS_NEW,
            ]);
        }
    }

    /**
     * Cancel expired schedule
     *
     * @param $data
     * @return void
     */
    public function cancel($data) {
        $scheduled = $this->getActiveScheduled($data);

        if($scheduled) {
            $scheduled->update([
                'status' => Bookmettings::STATUS_CANCEL,
            ]);
        }
    }

    /**
     * Get activated schedule.
     *
     * @param $data
     * @return mixed
     */
    public function getActiveScheduled($data) {
        return Bookmettings::where('email', $data['email'])
            ->where('status', Bookmettings::STATUS_NEW)
            ->first();
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function receiveWebhookOrderPaid(Request $request) {
        $data           = [];
        $webhookData    = $request->all();

        $data['order_confirmed']  = $webhookData['order_confirmed'] ?? null;
        $data['cancel_reason']    = $webhookData['cancel_reason'] ?? null;
        $data['financial_status'] = $webhookData['financial_status'] ?? null;
        $data['order_status_url'] = $webhookData['order_status_url'] ?? null;
        $data['email']            = $webhookData['email'] ?? null;
        $data['phone']            = $webhookData['phone'] ?? null;
        $data['processed_at']     = $webhookData['processed_at'] ?? null;

        return $data;
    }
}
