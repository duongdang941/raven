<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BookmettingsRequest;
use App\Mail\EmailBooking;
use App\Models\Bookmettings;
use App\Models\GeneralSettings;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

/**
 * Class BookmettingsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class BookmettingsCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Bookmettings::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/bookmettings');
        CRUD::setEntityNameStrings('bookmettings', 'bookmettings');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('email');
        CRUD::column('date_meting');
        $this->crud->addColumn([
            'name'  => 'status',
            'label' => 'Status',
            'type'  => 'boolean',
            'options' => [0 => 'NEW', 1 => 'PENDING', 2 => 'COMPLETED', 3 => 'CANCEL']
        ]);
        CRUD::column('created_at');
        CRUD::column('updated_at');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $daysForSchedule = GeneralSettings::where('setting_key', GeneralSettings::DAYS_AVAILABLE_SCHEDULE)->first();
        $availableDays   = $daysForSchedule
                            ? Carbon::now()->addDays($daysForSchedule->getAttribute('setting_value'))
                            : Carbon::now()->addDays(Bookmettings::DEFAULT_DAYS_SCHEDULE);

        CRUD::field('date_meting')->validationRules('after:' . $availableDays);
        CRUD::field('email');
        CRUD::addField([
            'name'  => 'status',
            'label' => 'Status',
            'type'  => 'select_from_array',
            'allows_null' => false,
            'options' => [0 => 'NEW', 1 => 'PENDING', 2 => 'COMPLETED', 3 => 'CANCEL'],
            'default' => 0,
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

        Bookmettings::updated(function ($entry) {
            $data = [];
            $intakeFormUrl = GeneralSettings::where('setting_key', GeneralSettings::INTAKE_FORM_URL)->first();

            $data['adminReschedule'] = true;
            $data['date_meting']     = $entry->date_meting;
            $data['linkReschedule']  = env('APP_URL') . '/schedule-form/?' . $entry->email;
            $data['googleFormUrl']   = $intakeFormUrl->getAttribute('setting_value');

            Mail::to($entry->email)->send(new EmailBooking(Bookmettings::STATUS_SUBMITTED_BOOKING, $data));
        });
    }

    /**
     * Define what happens when preview action enabled.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-show
     * @return void
     */
    protected function setupShowOperation()
    {
        $this->setupListOperation();
    }
}
