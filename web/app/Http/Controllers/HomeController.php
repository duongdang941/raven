<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show form reschedule for customer.
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function scheduleForm(Request $request) {
        $email = $request->get('email') ?? '';

        return view('vendor.backpack.custom.form', ['email' => $email]);
    }
}
