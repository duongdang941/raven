<?php

namespace App\Mail;

use App\Models\Bookmettings;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailBooking extends Mailable
{
    use Queueable, SerializesModels;

    protected $type;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($type, $data)
    {
        $this->type = $type;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        try {
            if ($this->type == Bookmettings::STATUS_BEFORE_BOOKING) {
                return $this->view('emails.booking', $this->data);
            }
            if ($this->type == Bookmettings::STATUS_SUBMITTED_BOOKING) {
                return $this->view('emails.submitted', $this->data);
            }
            if ($this->type == Bookmettings::STATUS_RESUBMITTED_BOOKING) {
                return $this->view('emails.resubmitted', $this->data);
            }
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage());
        }

    }
}
