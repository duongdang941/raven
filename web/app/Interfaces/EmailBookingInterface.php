<?php

namespace App\Interfaces;

interface EmailBookingInterface
{
    /**
     * sendmail function.
     *
     * @param array $params
     *
     * @return void
     */
    public function sendmail(array $params);
}