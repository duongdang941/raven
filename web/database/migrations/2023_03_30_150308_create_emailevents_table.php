<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmaileventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emailevents', function (Blueprint $table) {
            $table->id();
            $table->string('event');
            $table->bigInteger('id_email')->unsigned();
            $table->timestamps();

            $table->foreign('id_email')->references('id')->on('emailtemplates')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emailevents');
    }
}
