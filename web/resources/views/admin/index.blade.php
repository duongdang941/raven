@extends('layouts.admin')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    Welcome {{ Auth::user()->name }}
                </div>
            </div>
        </div>
    </div>
@endsection
