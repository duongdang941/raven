@extends(backpack_view('layouts.plain'))
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-10 offset-2">
                <div class="panel panel-default">
                    <div class="widget-form repeatable-element col-md-8 pt-3 pb-3">
                        <h2 class="d-flex flex-column">{{ __('Reschedule Form') }}</h2>
                        <form method="post" action="" id="rescheduleForm" name="embed-form" target="_self">
                            @csrf
                            <label for="datetime">{{ __('Please select a new booking') }}</label>
                            <input type="datetime-local" id="datetime" name="datetime" class="form-control mt-3 mb-3" required>
                            <input type="hidden" id="verifyEmail" name="verifyEmail" class="form-control mt-3 mb-3" value="{{ $email }}">
                            <div class="mailing-submit">
                                <button type="button" class="submit btn btn-success" id="btnReschedule" value="Schedule">Schedule</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function formData() {
            const form = document.getElementById('rescheduleForm');
            const formInputs = form.getElementsByTagName("input");
            let formData = new FormData();

            for (let input of formInputs) {
                formData.append(input.name, input.value);
            }

            return formData;
        }

        function doAjax(ajaxUrl, args) {
            $.ajax({
                type: 'POST',
                url: ajaxUrl,
                crossDomain: true,
                data: args,
                processData: false,
                contentType: false,
                success: function (data) {
                    new Noty({
                        type: "success",
                        text: data.message,
                    }).show();
                },
                error: function (data) {
                    new Noty({
                        type: "error",
                        text: data.message,
                    }).show();
                }
            });

            return false;
        }

        const btnSubmit = document.getElementById('btnReschedule');

        btnSubmit.addEventListener('click', function () {
            var ajaxUrl   = '{{ env('APP_URL') }}' + '/api/reschedule';
            let args = formData();

            doAjax(ajaxUrl, args);

            return false;
        });
    </script>
@endsection
