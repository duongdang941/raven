{{-- This file is used to store sidebar items, inside the Backpack admin panel --}}
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>

<li class="nav-item"><a class="nav-link" href="{{ backpack_url('bookmettings') }}"><i class="nav-icon la la-vimeo"></i> Book Meting</a></li>
{{--<li class="nav-item"><a class="nav-link" href="{{ backpack_url('emailevents') }}"><i class="nav-icon la la-calendar"></i> Email Events</a></li>--}}
{{--<li class="nav-item"><a class="nav-link" href="{{ backpack_url('emailtemplate') }}"><i class="nav-icon la la-envelope"></i> Email Templates</a></li>--}}
{{--<li class="nav-item"><a class="nav-link" href="{{ backpack_url('form-template') }}"><i class="nav-icon la la-wpforms"></i> Form Templates</a></li>--}}
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('general-settings') }}"><i class="nav-icon la la-cog"></i> General settings</a></li>